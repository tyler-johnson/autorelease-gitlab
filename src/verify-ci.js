import {isRegExp} from "lodash";

export default function(r, {branch}) {
	if (process.env.GITLAB_CI !== "true") {
		throw new Error("This is not running on Gitlab CI and therefore a new version won't be published.");
	}

	// Gitlab does not run builds on merge requests yet
	// https://gitlab.com/gitlab-org/gitlab-ce/issues/4176
	// if (has(process.env, "TRAVIS_PULL_REQUEST") && process.env.TRAVIS_PULL_REQUEST !== "false") {
	// 	throw new Error("This test run was triggered by a pull request and therefore a new version won't be published.");
	// }

	if (process.env.CI_BUILD_TAG) {
		throw new Error("This test run was triggered by a git tag and therefore a new version won't be published.");
	}

	if (branch) {
		let current = process.env.CI_BUILD_REF_NAME;
		let pass = [].concat(branch).some(b => {
			if (isRegExp(b)) {
				return b.test(current);
			} else if (typeof b === "string") {
				return current === b;
			} else if (typeof b === "function") {
				return b(current);
			}
		});

		if (!pass) {
			throw new Error(`This autorelease was triggered on branch ${current}, which is not a branch autorelease is configured to publish from.`);
		}
	}
}
