import _gitHead from "git-head";
import promisify from "es6-promisify";
import {resolve} from "path";
import parseGithubUrl from "parse-github-url";
import _request from "request";

const gitHead = promisify(_gitHead);
const request = promisify(_request);

export default async function(log, {
	basedir = ".",
	package: pkg,
	gitlabToken = process.env.GL_TOKEN,
	gitlabUrl = process.env.GL_URL
}) {
	let head = await gitHead(resolve(basedir, ".git"));
	let {repo} = parseGithubUrl(pkg.repository.url);

	let [resp,body] = await request({
		url: `/api/v3/projects/${encodeURIComponent(repo)}/repository/tags`,
		baseUrl: gitlabUrl || "https://gitlab.com",
		method: "POST",
		headers: {
			Authorization: "Bearer " + gitlabToken
		},
		qs: {
			tag_name: "v" + pkg.version,
			ref: head
		},
		json: true,
		body: {
			release_description: log
		}
	});

	if (resp.statusCode >= 400) throw body;
}
