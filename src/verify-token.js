export default function(r, {gitlabToken,package:pkg}) {
	if (!pkg.repository || !pkg.repository.url) {
		throw new Error(`No "repository" found in package.json.`);
	}

	if (!gitlabToken && !process.env.GL_TOKEN) {
		throw new Error("No gitlab token specified.");
	}
}
